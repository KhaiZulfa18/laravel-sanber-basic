<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\RedirectResponse;

class QuestionsController extends Controller
{
    public function index()
    {
        $data['headertitle'] = 'Questions';
        $data['title'] = 'Questions';
        $data['questions'] = DB::table('questions')->get();
        
        return view('pages/question/v_questions',$data);
    }

    public function create()
    {
        $data['headertitle'] = 'Create Questions';
        $data['title'] = 'Create Questions';
        return view('pages/question/v_create_questions',$data);
    }

    public function store(Request $request)
    {
        $input['judul'] = $request['judul'];
        $input['isi'] = $request['isi'];
        $query = DB::table('questions')->insert($input);

        if($query){
            $status = "Succes Insert Questions";
        }else{
            $status = "Ooopss Sorry, error.";
        }
        
        return redirect('/pertanyaan')->with('status',$status);
    }

    public function show($id)
    {
        $data['questions'] = DB::table('questions')->where('id',$id)->get();
        $data['headertitle'] = 'Show Question';
        $data['title'] = 'Show Question';

        return view('pages/question/v_show_questions', $data);
    }
    
    public function edit($id)
    {
        $data['questions'] = DB::table('questions')->where('id',$id)->get();
        $data['headertitle'] = 'Edit Question';
        $data['title'] = 'Edit Question';

        return view('pages/question/v_edit_questions', $data);
    }

    public function update(Request $request)
    {
        $update['judul'] = $request['judul']; 
        $update['isi'] = $request['isi'];
        $id = $request['id'];
        
        $action = DB::table('questions')->where('id',$id)->update($update);

        if($action){
            return redirect('/pertanyaan/'.$id)->with('statusSuccess','Questions Updated!');
        }else{
            return redirect('/pertanyaan/'.$id.'/edit')->with('statusError','Oppss... Error when your update this!');
        }
    }

    public function destroy($id)
    {   
        $action = DB::table('questions')->where('id', $id)->delete();

        if($action){
            return redirect('/pertanyaan')->with('statusDelete','Questions Deleted');
        }else{
            return redirect('/pertanyaan')->with('statusDelete','Oopsss.... Question Couldnt Deleted!');
        }
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function home()
    {
        return view('home');
    }

    public function tables()
    {
        $data['headertitle'] = 'Table';
        $data['title'] = 'Table';

        return view('pages.table')->with($data);
    }

    public function datatables()
    {
        $data['headertitle'] = 'Datatables';
        $data['title'] = 'Datatables';

        return view('pages.datatable')->with($data);
    }
}

@extends('layout.master')

@section('content')
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Blank Page</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Blank Page</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="card">
      <div class="card-header">
        @if (session('status'))
          <div class="alert alert-success">{{ session('status') }}</div>            
        @endif
        @if (session('statusDelete'))
          <div class="alert alert-success">{{ session('statusDelete') }}</div>            
        @endif
        
      </div>
      <!-- /.card-header -->
      <div class="card-body">
        <table id="example1" class="table table-bordered table-striped">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>Judul</th>
                    <th>Isi</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                @foreach($questions as $row)
                <tr>
                    <td>{{ $row->id }}</td>
                    <td>{{ $row->judul }}</td>
                    <td>{{ $row->isi }}</td>
                    <td class="text-center">
                        <div class="btn-group">
                            <a href="{{ url('/pertanyaan/'.$row->id)}}" type="button" class="btn btn-success"><i class="fas fa-eye"></i></a>
                        </div>
                    </td>
                </tr>
                @endforeach
          </tbody>
        </table>
        </div>
        </div>
    </section>
    <!-- /.content -->
@endsection


@push('script')
<script src="{{ asset('/adminlte/plugins/datatables/jquery.dataTables.js') }}"></script>
<script src="{{ asset('/adminlte/plugins/datatables-bs4/js/dataTables.bootstrap4.js') }}"></script>
<script>
  $(function () {
    $("#example1").DataTable();
  });
</script>
@endpush
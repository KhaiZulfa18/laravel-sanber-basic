@extends('layout.master')

@section('content')
<!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>{{ $title }}</h1>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <!-- left column -->
          <div class="col-md-12">
            <!-- general form elements -->
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">{{ $title }}</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              @foreach ($questions as $row)    
              <form role="form" action="{{ url('/pertanyaan/'.$row->id) }}" method="post">
                @csrf
                <div class="card-body">
                  <div class="form-group">
                    <label for="judul">Judul</label>
                    <input type="text" class="form-control" id="judul" name="judul" placeholder="Judul" value="{{ $row->judul }}">
                    <input type="hidden" name="id" value="{{ $row->id }}"> <br/>
                  </div>
                  <div class="form-group">
                    <label for="isi">Isi</label>
                    <input type="text" class="form-control" id="isi" name="isi" placeholder="Isi Pertanyaan" value="{{$row->isi}}">
                  </div>
                </div>
                <!-- /.card-body -->

                @method('PUT')

                <div class="card-footer">
                    @if (session('statusError'))
                    <div class="alert alert-danger">{{ session('statusError') }}</div>            
                    @endif
                  <button type="submit" class="btn btn-primary">Update</button>
                </div>
              </form>
              @endforeach
            </div>
            <!-- /.card -->
            </div>
        </div>
    </div>
<section>
@endsection
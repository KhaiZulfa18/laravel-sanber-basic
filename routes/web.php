<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
//Tugas Route
Route::get('/', 'HomeController@home');
Route::get('/register', 'AuthController@register');
Route::post('/welcome', 'AuthController@welcome');

//Tugas Templating Blade
Route::get('/tables', 'HomeController@tables');

Route::get('/data-tables', 'HomeController@datatables');

//Tugas CRUD
Route::get('/pertanyaan','QuestionsController@index');
Route::get('/pertanyaan/create','QuestionsController@create');
Route::post('/pertanyaan','QuestionsController@store');
Route::get('/pertanyaan/{id}','QuestionsController@show');
Route::get('/pertanyaan/{id}/edit','QuestionsController@edit');
Route::put('/pertanyaan/{id}','QuestionsController@update');
Route::delete('/pertanyaan/{id}','QuestionsController@destroy');
